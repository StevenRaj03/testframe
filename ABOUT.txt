ETU001577 
RAJAOFEREA Maharo Steven
P14B n°55

**Si vous souhaitez créer un nouveau projet:
- créez un projet java web application
- ajouter la classe Peronne.java (/src/java/modele)
- ajouter les pages web nécessaires (/web)
- importez les librairies nécessaires (/build/web/WEB-INF/lib) :
	- annotations-2.0.1.jar
	- GenericServlet.jar
	- guava-15.0.jar
	- javassist-3.19.0-GA.jar
	- reflections-0.9.10.jar

**Sinon
- lancez ce projet
- tapez "personne-all.do" sur l'url pour aller à la page de liste des peronnes
- tapez "personne-add.do" sur l'url pour aller à la page d'ajout de personne

